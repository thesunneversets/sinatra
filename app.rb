#!/usr/bin/env ruby
#encoding: UTF-8

require "rubygems"
require "sinatra"
require "data_mapper"
require_relative "bookmark"
require "dm-serializer"

DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/bookmarks.db")
DataMapper.finalize.auto_migrate!

get "/hello" do
  "Hello, Sinatra!"
end

def get_all_bookmarks
  Bookmark.all(:order => :title)
end
get "/bookmarks" do
  content_type :json
  get_all_bookmarks.to_json
end

